# PortMidi Song Finisher

Simple example written to test just what you can do with PortMidi. Turns out
you can actually make it continue playing "Twinkle Twinkle Little Star" if you
play the beginning. This is amazing!
