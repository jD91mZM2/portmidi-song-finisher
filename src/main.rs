use failure::Error;
use portmidi::{DeviceInfo, PortMidi, MidiMessage};
use std::{
    io::{self, prelude::*},
    thread,
    time::Duration
};

const BUF_SIZE: usize = 1024;
const KEY_STOP: u8 = 96;
const STRENGTH: u8 = 50;

const STATUS_KEY: u8 = 0x80;
const STATUS_ON: u8 = 0x10;

const TWINKLE_TWINKLE_START: &[u8] = &[
    0, 7, 7, 9, 9, 7
];
const TWINKLE_TWINKLE_END: &[u8] = &[
    5, 5, 4, 4, 2, 2, 0
];

fn main() -> Result<(), Error> {
    let midi = PortMidi::new()?;

    let devices = midi.devices()?;

    devices.iter()
        .filter(|device| device.is_input())
        .for_each(|device| println!("{} = {}", device.id(), device.name()));

    let input = prompt_info(&devices, "Input device ID: ")?;
    let mut input = midi.input_port(input, BUF_SIZE)?;

    devices.iter()
        .filter(|device| device.is_output())
        .for_each(|device| println!("{} = {}", device.id(), device.name()));

    let output = prompt_info(&devices, "Output device ID: ")?;
    let mut output = midi.output_port(output, BUF_SIZE)?;

    let mut i = 0;
    let mut start = None;

    loop {
        let event = match input.read()? {
            Some(event) => event,
            None => {
                thread::sleep(Duration::from_secs(1));
                continue;
            }
        };
        let msg = event.message;
        if msg.status & STATUS_KEY != STATUS_KEY {
            continue;
        }
        let key = msg.data1;
        let on = msg.status & STATUS_ON == STATUS_ON;

        println!("{:?} {:?}", msg, start);

        if key == KEY_STOP {
            break;
        }

        if !on {
            if start.map(|start| key == start + TWINKLE_TWINKLE_START[i]).unwrap_or(false) {
                i += 1;

                if i == TWINKLE_TWINKLE_START.len() {
                    i = 0;

                    println!("Detected song!");

                    thread::sleep(Duration::from_secs(1));

                    let start = start.take().unwrap();

                    for &key in TWINKLE_TWINKLE_END {
                        output.write_message(MidiMessage {
                            status: STATUS_KEY | STATUS_ON,
                            data1: start + key,
                            data2: STRENGTH,
                            data3: 0,
                        })?;
                        thread::sleep(Duration::from_millis(500));
                        output.write_message(MidiMessage {
                            status: STATUS_KEY,
                            data1: start + key,
                            data2: 0,
                            data3: 0,
                        })?;
                        thread::sleep(Duration::from_millis(500));
                    }
                }
            } else {
                i = 0;
                start = Some(key);
            }
        }
    }

    Ok(())
}
fn prompt_info(devices: &[DeviceInfo], prompt: &str) -> Result<DeviceInfo, Error> {
    let mut id = String::new();

    loop {
        print!("{}", prompt);
        io::stdout().flush()?;

        io::stdin().read_line(&mut id)?;

        let id = match id.trim().parse() {
            Ok(id) => id,
            Err(_) => { eprintln!("invalid number"); continue; }
        };

        match devices.iter().find(|device| device.id() == id) {
            Some(device) => break Ok(device.clone()),
            None => { eprintln!("no such device"); continue; }
        }
    }
}
